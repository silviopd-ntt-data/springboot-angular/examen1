package Model;

public class CuentaCorriente extends CuentaBancaria {

    private double interesProrrateado;

    public CuentaCorriente(String numeroCuenta, double montoApertura, Cliente cliente, double interesProrrateado) {
        super(numeroCuenta, montoApertura, cliente);
        this.interesProrrateado = interesProrrateado;
    }

    public double getInteresProrrateado() {
        return interesProrrateado;
    }

    public void setInteresProrrateado(double interesProrrateado) {
        this.interesProrrateado = interesProrrateado;
    }

    public double calcularSaldo() {
        return getMontoApertura() + (getMontoApertura() * getInteresProrrateado());
    }

    @Override
    public String toString() {
        return "CuentaCorriente{" +
                "cliente=" + getCliente() +
                ", montoApertura=" + getMontoApertura() +
                ", interesProrrateado=" + interesProrrateado +
                ", saldo=" + calcularSaldo() +
                '}';
    }
}
