package Model;

public abstract class CuentaBancaria {
    private String numeroCuenta;
    private double montoApertura;
    private double interesProrrateado;
    private Cliente cliente;

    public CuentaBancaria(String numeroCuenta, double montoApertura, Cliente cliente) {
        this.numeroCuenta = numeroCuenta;
        this.montoApertura = montoApertura;
        this.cliente = cliente;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public double getMontoApertura() {
        return montoApertura;
    }

    public void setMontoApertura(double montoApertura) {
        this.montoApertura = montoApertura;
    }

    public double getInteresProrrateado() {
        return interesProrrateado;
    }

    public void setInteresProrrateado(double interesProrrateado) {
        this.interesProrrateado = interesProrrateado;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public double calcularSaldo() {
        return montoApertura;
    }
}
