import Model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void contarCuentasPorHombreMujer(List<CuentaCorriente> cuentaCorrienteList, List<CuentaAhorros> cuentaAhorrosList) {
        //Elaborar un método que permita, de dos listas de cuentas CC y CA, mostrar cuantas mujeres y cuantos hombres tiene una cuenta.
        for (Sexo sexo : Sexo.values()) {
            System.out.println("CUENTA CORRIENTE - " + sexo);
//            System.out.println("Cuentas corrientes "+sexo+": "+cuentaCorrienteList.stream().filter(c -> c.getCliente().getSexo() == sexo).collect(Collectors.toList()));
            System.out.println("Cuantas cuentas corrientes " + sexo + ": " + cuentaCorrienteList.stream().filter(c -> c.getCliente().getSexo() == sexo).count());

            System.out.println("");

            System.out.println("CUENTA AHORRO - " + sexo);
//            System.out.println("Cuentas ahorros  "+sexo+": "+cuentaAhorrosList.stream().filter(c -> c.getCliente().getSexo() == sexo).collect(Collectors.toList()));
            System.out.println("Cuantas cuentas ahorros   " + sexo + ": " + cuentaAhorrosList.stream().filter(c -> c.getCliente().getSexo() == sexo).count());

            System.out.println("");
        }
    }

    public static void contarCuentasPorProfesion(List<CuentaCorriente> cuentaCorrienteList, List<CuentaAhorros> cuentaAhorrosList) {
        //Elaborar un método que permita, de dos listas de cuentas CC y CA, cuantas cuentas CC y cuantas cuentas CA por profesión hay.
        for (Profesion profesion : Profesion.values()) {
            System.out.println("CUENTA CORRIENTE " + profesion);
//            System.out.println("Cuentas corrientes "+profesion+": "+cuentaCorrienteList.stream().filter(c -> c.getCliente().getProfesion() == profesion).collect(Collectors.toList()));
            System.out.println(profesion + ":" + cuentaCorrienteList.stream().filter(c -> c.getCliente().getProfesion() == profesion).count());

            System.out.println("");

            System.out.println("CUENTA AHORRO " + profesion);
//            System.out.println("Cuentas ahorros  "+profesion+": "+cuentaAhorrosList.stream().filter(c -> c.getCliente().getProfesion() == profesion).collect(Collectors.toList()));
            System.out.println(profesion + ":" + cuentaAhorrosList.stream().filter(c -> c.getCliente().getProfesion() == profesion).count());

            System.out.println("");
        }
    }

    public static void main(String[] args) {

        List<CuentaCorriente> cuentaCorrienteList = new ArrayList();
        List<CuentaAhorros> cuentaAhorrosList = new ArrayList();

        //creamos usuarios cuenta corriente
        Cliente usuario1 = new Cliente("70840722", "Carlos H.", "Vazques Casas", 20, Sexo.Masculino, Profesion.Ingeniero);
        CuentaCorriente cuentaCorriente1 = new CuentaCorriente("CC00001", 100, usuario1, 0.5);

        Cliente usuario2 = new Cliente("43080722", "Jean H.", "Vazques Casas", 30, Sexo.Masculino, Profesion.Abogado);
        CuentaCorriente cuentaCorriente2 = new CuentaCorriente("CC00002", 200, usuario2, 1.5);

        Cliente usuario3 = new Cliente();
        usuario3.setDni("12345678");
        usuario3.setNombre("Dora");
        usuario3.setApellidos("Herrea");
        usuario3.setEdad(40);
        usuario3.setSexo(Sexo.Femenino);
        usuario3.setProfesion(Profesion.Ingeniero);
        CuentaCorriente cuentaCorriente3 = new CuentaCorriente("CC00005", 300, usuario3, 2.5);


        //agregamos usuarios a la lista
        cuentaCorrienteList.add(cuentaCorriente1);
        cuentaCorrienteList.add(cuentaCorriente2);
        cuentaCorrienteList.add(cuentaCorriente3);


        //creamos usuarios cuenta ahorros
        Cliente usuario5 = new Cliente("40845722", "Alberto H.", "Torres Casas", 50, Sexo.Masculino, Profesion.Contador);
        CuentaAhorros cuentaAhorros1 = new CuentaAhorros("CA00001", 400, usuario5, 0.5, 10);

        Cliente usuario6 = new Cliente("70845722", "Juana H.", "Diaz Casas", 60, Sexo.Femenino, Profesion.Medico);
        CuentaAhorros cuentaAhorros2 = new CuentaAhorros("CA00002", 500, usuario6, 1.5, 20);

        Cliente usuario7 = new Cliente();
        usuario7.setDni("43345678");
        usuario7.setNombre("Juana");
        usuario7.setApellidos("Cordova");
        usuario7.setEdad(70);
        usuario7.setSexo(Sexo.Femenino);
        usuario7.setProfesion(Profesion.Ingeniero);
        CuentaAhorros cuentaAhorros3 = new CuentaAhorros("CA00003", 600, usuario7, 2.5, 30);

        //agregamos usuarios a la lista
        cuentaAhorrosList.add(cuentaAhorros1);
        cuentaAhorrosList.add(cuentaAhorros2);
        cuentaAhorrosList.add(cuentaAhorros3);


        //Elaborar un método que permita, de dos listas de cuentas CC y CA, mostrar cuantas mujeres y cuantos hombres tiene una cuenta.
        contarCuentasPorHombreMujer(cuentaCorrienteList, cuentaAhorrosList);

        System.out.println("");
        System.out.println("");

        //Elaborar un método que permita, de dos listas de cuentas CC y CA, cuantas cuentas CC y cuantas cuentas CA por profesión hay.
        contarCuentasPorProfesion(cuentaCorrienteList, cuentaAhorrosList);

    }
}
